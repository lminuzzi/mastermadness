package com.lpm.mastermadness.repositories

import com.lpm.mastermadness.entities.Game
import com.lpm.mastermadness.entities.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull
import java.util.function.IntFunction
import java.util.stream.Collectors
import java.util.stream.IntStream

@DataJpaTest
class GameRepositoryTest @Autowired constructor(
        val entityManager: TestEntityManager,
        val gameRepository: GameRepository) {

    @Test
    fun `When findByIdOrNull then return Game`() {
        val user = User("testuser")
        entityManager.persist(user)
        val game = Game(user, "13524", 12, 6, 4)
        entityManager.persist(game)
        entityManager.flush()
        val found = gameRepository.findByIdOrNull(game.id!!)
        assertThat(found).isEqualTo(game)
    }
}