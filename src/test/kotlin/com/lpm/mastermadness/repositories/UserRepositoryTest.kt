package com.lpm.mastermadness.repositories

import com.lpm.mastermadness.entities.Game
import com.lpm.mastermadness.entities.User
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager
import org.springframework.data.repository.findByIdOrNull

@DataJpaTest
class UserRepositoryTest @Autowired constructor(
        val entityManager: TestEntityManager,
        val userRepository: UserRepository) {

    @Test
    fun `When findByLogin then return User`() {
        val userTest = User("testuser2")
        entityManager.persist(userTest)
        entityManager.flush()
        val user = userRepository.findByName(userTest.name)
        assertThat(userTest).isEqualTo(user)
    }
}