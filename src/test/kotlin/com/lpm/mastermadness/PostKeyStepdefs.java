package com.lpm.mastermadness;

import com.lpm.mastermadness.entities.Game;
import com.lpm.mastermadness.entities.GamePost;
import com.lpm.mastermadness.entities.GameStatus;
import com.lpm.mastermadness.entities.User;
import com.lpm.mastermadness.repositories.GameRepository;
import com.lpm.mastermadness.services.GamePostService;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.assertj.core.api.Assertions.assertThat;

public class PostKeyStepdefs extends BaseStepsdefs {
    private static final int RADIX = 10;
    private String solution;
    private char[] contentPost;
    private char[] helperSlot;
    private Game game;
    private GamePost gamePost;
    private ResponseEntity<Game> responseEntity;

    @Autowired
    GameRepository gameRepository;

    @Autowired
    GamePostService gamePostService;

    @Given("I want to solve the problem")
    public void iWantToSolveTheProblem() {
        createInitialGame();
    }

    private void createInitialGame() {
        this.game = postStart(new User("user", null)).getBody();
        assertThat(this.game).isNotNull();
        int solutionSize = Objects.requireNonNull(this.game.getSolutionSize());
        contentPost = new char[solutionSize];
        helperSlot = new char[solutionSize];
        Optional<Game> optionalGame = gameRepository.findById(
                Objects.requireNonNull(Objects.requireNonNull(this.game).getId()));
        assertThat(optionalGame.isPresent()).isTrue();
        Game gameDatabase = optionalGame.get();
        this.solution = gameDatabase.getSolution();
    }

    @When("I post the correct key")
    public void iPostTheCorrectKey() {
        this.contentPost = this.solution.toCharArray();
    }

    @And("I push the post button")
    public void iPushThePostButton() {
        pushPostButton();
    }

    private void pushPostButton() {
        this.gamePost = new GamePost(fillEmptyIndexes(this.contentPost, '0'), LocalDateTime.now(),
                null, this.game, null);
        //this.responseEntity = postKey(this.gamePost);
        this.responseEntity = gamePostService.save(this.gamePost);
    }

    private String fillEmptyIndexes(char[] charContent, char value) {
        IntStream.range(0, charContent.length)
                .filter(index -> charContent[index] == 0)
                .forEach(index -> charContent[index] = value);
        return new String(charContent);
    }

    @Then("The game must be created with status CREATED")
    public void theGameMustBeCreatedWithStatusCREATED() {
        assertThat(this.responseEntity).isNotNull();
        assertThat(this.responseEntity.getStatusCode()).isEqualTo(HttpStatus.OK);

        this.game = this.responseEntity.getBody();
        this.gamePost = Objects.requireNonNull(Objects.requireNonNull(this.game).getGamePosts())
                .get(Objects.requireNonNull(this.game.getGamePosts()).size()-1);
        assertThat(Objects.requireNonNull(gamePost).getContentPost())
                .isNotEmpty()
                .hasSize(SOLUTION_SIZE);
    }

    @And("All the positions in the additional helpers slots must to be filled with the value {int}")
    public void noneOfTheAdditionalHelpersSlotsMustToBeFilled(int helperSlotValue) {
        assertThat(Objects.requireNonNull(this.gamePost.getHelperSlots()).length()).isEqualTo(SOLUTION_SIZE);
        assertThat(this.gamePost.getHelperSlots()).isEqualTo(getContent(String.valueOf(helperSlotValue)));
    }

    @And("The game must be finished with status {string}")
    public void theGameMustBeFinished(String status) {
        this.game = this.responseEntity.getBody();
        assertThat(Objects.requireNonNull(this.game).getStatus()).isInstanceOf(GameStatus.class);
        assertThat(this.game.getStatus()).isEqualTo(GameStatus.valueOf(status));
    }

    @NotNull
    private String getContent(String charValue) {
        return IntStream.range(0, Objects.requireNonNull(this.game.getSolutionSize()))
                .mapToObj(index -> charValue.substring(0, 1)).collect(Collectors.joining());
    }

    @When("I post the incorrect key")
    public void iPostTheIncorrectKey() {
        this.contentPost = getContent("0").toCharArray();
    }

    @And("The game must not be finished and keep the status CREATED")
    public void theGameMustNotBeFinishedAndKeepTheStatusCREATED() {
        assertThat(Objects.requireNonNull(this.gamePost.getGame()).getStatus()).isInstanceOf(GameStatus.class);
        assertThat(this.contentPost).isNotEqualTo(this.solution);
        assertThat(this.gamePost.getGame().getStatus()).isEqualTo(GameStatus.CREATED);
    }

    @When("I post these entries correct keys")
    public void iPostTheseEntriesCorrectKeys(List<Integer> entriesPositions) {
        entriesPositions.forEach(position -> this.contentPost[position - 1] = this.solution.charAt(position - 1));
    }

    @And("The additional helpers slots must to be filled with the value {int} in these positions")
    public void theAdditionalHelpersSlotsMustToBeFilledWithTheValueInThesePositions(int helperSlotValue,
                                                                                    List<Integer> entriesPositions) {
        assertThat(Objects.requireNonNull(this.gamePost.getHelperSlots()).length()).isEqualTo(SOLUTION_SIZE);
        entriesPositions.forEach(position -> {
            this.helperSlot[position - 1] = Character.forDigit(helperSlotValue, RADIX);
            assertThat(this.gamePost.getHelperSlots().charAt(position - 1)).isEqualTo(this.helperSlot[position - 1]);
        });
    }

    @And("The others positions in the additional helpers slots must to be filled with the value {int}")
    public void theOthersPositionsInTheAdditionalHelpersSlotsMustToBeFilledWithTheValue(int helperSlotValue) {
        this.helperSlot = fillEmptyIndexes(this.helperSlot, Character.forDigit(helperSlotValue, RADIX)).toCharArray();
        assertThat(this.gamePost.getHelperSlots()).isEqualTo(new String(this.helperSlot));
    }

    @When("I post in these entries a value cointained in the correct key, but not in the same position")
    public void iPostInTheseEntriesAValueCointainedInTheCorrectKeyButNotInTheSamePosition(
            List<Integer> entriesPositions) {
        int solutionSize = Objects.requireNonNull(this.game.getSolutionSize());
        for (Integer position : entriesPositions) {
            if (position == solutionSize) {
                this.contentPost[position - 1] = this.solution.charAt(0);
            } else {
                this.contentPost[position - 1] = this.solution.charAt(position);
            }
        }
    }

    @Given("I want to solve the problem in the try number {int}")
    public void iWantToSolveTheProblemInTheTryNumber(int tryNumber) {
        createInitialGame();
        tryToSolveByRound(tryNumber);
    }

    @Given("I want to solve the problem in the last round")
    public void iWantToSolveTheProblemInTheLastRound() {
        createInitialGame();
        tryToSolveByRound(Objects.requireNonNull(this.game.getNumRounds()));
    }

    private void tryToSolveByRound(int tryNumber) {
        this.contentPost = getContent("0").toCharArray();
        for (int index = 1; index < tryNumber; index++) {
            pushPostButton();
        }
        assertThat(this.responseEntity).isNotNull();
        assertThat(this.responseEntity.getBody()).isNotNull();

        this.game = this.responseEntity.getBody();
        this.gamePost = Objects.requireNonNull(this.game.getGamePosts()).get(this.game.getGamePosts().size()-1);
        assertThat(Objects.requireNonNull(gamePost).getHelperSlots())
                .isNotEmpty()
                .hasSize(SOLUTION_SIZE);
        assertThat(this.game).isNotNull();
        assertThat(this.game.getGamePosts()).isNotNull();
        assertThat(this.game.getGamePosts().size()).isEqualTo(tryNumber-1);
    }
}