package com.lpm.mastermadness;

import com.lpm.mastermadness.entities.Game;
import com.lpm.mastermadness.entities.GameStatus;
import com.lpm.mastermadness.entities.User;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.time.LocalDateTime;
import java.util.Objects;

import static org.assertj.core.api.Assertions.assertThat;

public class StartGameStepdefs extends SpringBootBaseIntegrationTest {
    private Game game;

    @Given("I want start a game")
    public void iWantStartAGame() {
        this.game = new Game(new User("", null), "", 12, 6, 4,
                null, GameStatus.CREATED, LocalDateTime.now(), null);
    }

    @When("I fill the input with my {string}")
    public void iFillTheInputWithMy(String username) {
        Objects.requireNonNull(this.game.getUser()).setName(username);
    }

    @And("I push the button start")
    public void iPushTheButtonStart() {
        this.game.setAddedAt(LocalDateTime.now());
    }

    @Then("I should receive the new Game for the user {string}")
    public void iShouldReceiveTheMessageGreatTheGameWasStarted(String username) {
        ResponseEntity<Game> responseEntity = postStart(this.game.getUser());
        Game gameCreated = responseEntity.getBody();
        assertThat(gameCreated).isNotNull();
        assertThat(gameCreated.getUser()).isNotNull();
        String usernameGame = gameCreated.getUser().getName();

        assertThat(responseEntity.getStatusCode().equals(HttpStatus.CREATED));
        assertThat(usernameGame).isEqualTo(username);
    }
}
