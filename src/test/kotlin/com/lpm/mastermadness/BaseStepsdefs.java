package com.lpm.mastermadness;

import com.lpm.mastermadness.entities.Game;
import com.lpm.mastermadness.entities.GamePost;
import com.lpm.mastermadness.entities.User;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

public class BaseStepsdefs {
    private static final Logger log = LoggerFactory.getLogger(SpringBootBaseIntegrationTest.class);

    private final String SERVER_URL = "http://localhost";
    private final String START_ENDPOINT = "/start";
    private final String POST_KEY_ENDPOINT = "/postkey";

    protected final int SOLUTION_SIZE = 4;

    private RestTemplate restTemplate;

    @LocalServerPort
    protected int port;

    public BaseStepsdefs() {
        restTemplate = new RestTemplate();
    }

    private String startEndpoint() {
        return getBaseUrl() + START_ENDPOINT;
    }

    private String postKeyEndpoint() {
        return getBaseUrl() + POST_KEY_ENDPOINT;
    }

    @NotNull
    private String getBaseUrl() {
        return SERVER_URL + ":" + port + "/api";
    }

    ResponseEntity<Game> postStart(User user) {
        return restTemplate.postForEntity(startEndpoint(), user, Game.class);
    }

    ResponseEntity<GamePost> postKey(GamePost gamePost) {
        return restTemplate.postForEntity(postKeyEndpoint(), gamePost, GamePost.class);
    }
}