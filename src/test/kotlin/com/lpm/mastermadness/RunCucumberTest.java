package com.lpm.mastermadness;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty", "html:target/cucumber"},
        features = "src/test/resources/com/lpm/mastermadness",
        glue = {"com.lpm.mastermadness", "cucumber.api.spring"})
public class RunCucumberTest {
}