Feature: Post a key in the game - Codebreaker
  For complete the objective of the game and try to solve the correct answer

  Scenario: Post the correct key
    Given I want to solve the problem
    When I post the correct key
    And I push the post button
    Then The game must be created with status CREATED
    And All the positions in the additional helpers slots must to be filled with the value 1
    And The game must be finished with status "SOLVED"

  Scenario: Post the incorrect key
    Given I want to solve the problem
    When I post the incorrect key
    And I push the post button
    Then The game must be created with status CREATED
    And All the positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the first entry correct key
    Given I want to solve the problem
    When I post these entries correct keys
      | 1 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 1 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the second entry correct key
    Given I want to solve the problem
    When I post these entries correct keys
      | 2 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 2 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the third entry correct key
    Given I want to solve the problem
    When I post these entries correct keys
      | 3 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 3 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the second and fourth entries correct key
    Given I want to solve the problem
    When I post these entries correct keys
      | 2 | 4 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 2 | 4 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the second, third and fourth entries correct key
    Given I want to solve the problem
    When I post these entries correct keys
      | 2 | 3 | 4 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 1 in these positions
      |2 | 3 | 4 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the first entry a contained solution correct key
    Given I want to solve the problem
    When I post in these entries a value cointained in the correct key, but not in the same position
      | 1 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 2 in these positions
      | 1 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the third entry a contained solution correct key
    Given I want to solve the problem
    When I post in these entries a value cointained in the correct key, but not in the same position
     | 3 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 2 in these positions
      | 3 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the third and fourth entries a contained solution correct key
    Given I want to solve the problem
    When I post in these entries a value cointained in the correct key, but not in the same position
      | 3 | 4 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 2 in these positions
      | 3 | 4 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the first and fourth entries a contained solution correct key, and the second entry correct key
    Given I want to solve the problem
    When I post in these entries a value cointained in the correct key, but not in the same position
      | 1 | 4 |
    And I post these entries correct keys
      | 2 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 2 in these positions
      | 1 | 4 |
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 2 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Post the first and fourth entries a contained solution correct key, and the second and third entries correct keys
    Given I want to solve the problem
    When I post in these entries a value cointained in the correct key, but not in the same position
      | 1 | 4 |
    And I post these entries correct keys
      | 2 | 3 |
    And I push the post button
    Then The game must be created with status CREATED
    And The additional helpers slots must to be filled with the value 2 in these positions
      | 1 | 4 |
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 2 | 3 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Second try - Post the first, third and fourth entries a contained solution correct key, and the second entry correct key
    Given I want to solve the problem in the try number 2
    When I post in these entries a value cointained in the correct key, but not in the same position
      | 1 | 3 | 4 |
    And I post these entries correct keys
      | 2 |
    And I push the post button
    Then The additional helpers slots must to be filled with the value 2 in these positions
      | 1 | 3 | 4 |
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 2 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Third try - Post the first, fourth entries a contained solution correct key, and the second and entries correct key
    Given I want to solve the problem in the try number 3
    When I post in these entries a value cointained in the correct key, but not in the same position
      | 1 | 4 |
    And I post these entries correct keys
      | 2 | 3 |
    And I push the post button
    Then The additional helpers slots must to be filled with the value 2 in these positions
      | 1 | 4 |
    And The additional helpers slots must to be filled with the value 1 in these positions
      | 2 | 3 |
    And The others positions in the additional helpers slots must to be filled with the value 0
    And The game must not be finished and keep the status CREATED

  Scenario: Fourth try - Post the correct key
    Given I want to solve the problem in the try number 4
    When I post these entries correct keys
      | 1 | 2 | 3 | 4 |
    And I push the post button
    Then All the positions in the additional helpers slots must to be filled with the value 1
    And The game must be finished with status "SOLVED"

  Scenario: Twelve try - Post the incorrect key and the game is closed
    Given I want to solve the problem in the last round
    When I post the incorrect key
    And I push the post button
    Then All the positions in the additional helpers slots must to be filled with the value 0
    And The game must be finished with status "CLOSED"
