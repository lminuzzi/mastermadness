Feature: Start the game
  Everybody wants to start this nice game

  Scenario Outline: Starting the game
    Given I want start a game
    When I fill the input with my "<name>"
    And I push the button start
    Then I should receive the new Game for the user "<name>"

    Examples:
      | name |
      | Samara |
      | Carrie |
      | Clarice |