package com.lpm.mastermadness

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class MastermadnessApplication

fun main(args: Array<String>) {
    runApplication<MastermadnessApplication>(*args)
}
