package com.lpm.mastermadness.repositories

import com.lpm.mastermadness.entities.GamePost
import org.springframework.data.repository.CrudRepository

interface GamePostRepository : CrudRepository<GamePost, Long> {
    fun findAllByOrderByAddedAtDesc(): Iterable<GamePost>
}