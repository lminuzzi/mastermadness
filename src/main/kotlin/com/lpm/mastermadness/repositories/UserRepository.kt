package com.lpm.mastermadness.repositories

import com.lpm.mastermadness.entities.User
import org.springframework.data.repository.CrudRepository

interface UserRepository : CrudRepository<User, Long> {
    fun findByName(name: String): User
}