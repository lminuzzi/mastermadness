package com.lpm.mastermadness.repositories

import com.lpm.mastermadness.entities.Game
import org.springframework.data.repository.CrudRepository

interface GameRepository : CrudRepository<Game, Long> {
    fun findAllByOrderByAddedAtDesc(): Iterable<Game>
}