package com.lpm.mastermadness.events

import com.lpm.mastermadness.entities.Game
import com.lpm.mastermadness.entities.GamePost
import com.lpm.mastermadness.services.GamePostService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/postkey")
class GameController {
    @Autowired
    lateinit var gamePostService: GamePostService

    @PostMapping
    @ResponseBody
    fun postKey(@RequestBody gamePost: GamePost): ResponseEntity<Game> {
        return gamePostService.save(gamePost)
    }
}