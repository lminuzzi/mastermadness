package com.lpm.mastermadness.events

import com.lpm.mastermadness.entities.Game
import com.lpm.mastermadness.entities.User
import com.lpm.mastermadness.services.GameService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/api/start")
class StartController {
    @Autowired
    lateinit var gameService: GameService

    @PostMapping
    @ResponseBody
    fun start(@RequestBody user: User): ResponseEntity<Game> {
        return gameService.startGame(user)
    }
}