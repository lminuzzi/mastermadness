package com.lpm.mastermadness.entities

import com.fasterxml.jackson.annotation.JsonBackReference
import com.fasterxml.jackson.annotation.JsonFormat
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "game_post")
data class GamePost(
        var contentPost: String,
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        var addedAt: LocalDateTime = LocalDateTime.now(),
        var helperSlots: String?,
        @ManyToOne(targetEntity = Game::class, fetch = FetchType.LAZY)
        @JoinColumn(name = "game_id")
        @JsonBackReference
        var game: Game? = null,
        @Id @GeneratedValue(strategy = GenerationType.IDENTITY) var id: Long? = null) {
    private constructor() : this("", LocalDateTime.now(),
            null, null, null)
}