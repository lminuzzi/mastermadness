package com.lpm.mastermadness.entities

import javax.persistence.*

@Entity
@Table(name = "user")
data class User(
        var name: String,
        @Id @GeneratedValue(strategy= GenerationType.IDENTITY) var id: Long? = null) {
    private constructor() : this("", null)
}