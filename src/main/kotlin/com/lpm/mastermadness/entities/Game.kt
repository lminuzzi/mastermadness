package com.lpm.mastermadness.entities

import com.fasterxml.jackson.annotation.*
import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "game")
data class Game(
        @ManyToOne var user: User?,
        var solution: String = "",
        @Column(name = "number_of_rounds")
        var numRounds: Int? = null,
        @Column(name = "number_of_choices")
        var numChoices: Int? = null,
        var solutionSize: Int? = null,
        @OneToMany(targetEntity = GamePost::class, mappedBy = "game",
                cascade = arrayOf(CascadeType.ALL), fetch = FetchType.EAGER)
        @JsonManagedReference
        var gamePosts: MutableList<GamePost>? = mutableListOf<GamePost>(),
        var status: GameStatus = GameStatus.CREATED,
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        var addedAt: LocalDateTime = LocalDateTime.now(),
        @Id @GeneratedValue(strategy=GenerationType.IDENTITY) var id: Long? = null) {
    private constructor() : this(User("", null), "")
}

enum class GameStatus {
    CREATED, SOLVED, CLOSED
}