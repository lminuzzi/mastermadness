package com.lpm.mastermadness.services

import com.lpm.mastermadness.entities.Game
import com.lpm.mastermadness.entities.GamePost
import com.lpm.mastermadness.entities.GameStatus
import com.lpm.mastermadness.repositories.GamePostRepository
import com.lpm.mastermadness.repositories.GameRepository
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class GamePostService(val gamePostRepository: GamePostRepository,
                      val gameRepository: GameRepository) {
    @Value("\${number.of.rounds:}")
    lateinit var numberOfRounds: Number

    @Value("\${size.of.solution:}")
    lateinit var solutionSize: Number

    fun save(gamePost: GamePost): ResponseEntity<Game> {
        val findById = gameRepository.findById(gamePost.game!!.id!!)
        if (!findById.isPresent) {
            return ResponseEntity.noContent().build()
        }
        var game: Game = findById.get()
        if (game.status != GameStatus.CREATED) {
            return ResponseEntity.badRequest().build()
        }
        //TODO these two lines can be executed in parallel
        val gamePostSaved = resolveGamePost(gamePost, game)
        game = resolveGame(gamePost, game)

        game.gamePosts?.add(gamePostSaved)
        //Client side don't have the solution
        game.solution = ""
        return ResponseEntity.ok(game)
    }

    fun getHelpSlots(contentPost: String, solution: String): String {
        var helpSlots = ""
        contentPost.toCharArray().withIndex().forEach { (index, value) ->
            helpSlots += when {
                solution[index] == value -> {
                    '1'
                }
                solution.contains(value) -> {
                    '2'
                }
                else -> {
                    '0'
                }
            }
        }
        return helpSlots.padEnd(solutionSize.toInt(), '0')
    }

    private fun resolveGame(gamePost: GamePost, gameParam: Game): Game {
        var game = gameParam
        if (gamePost.contentPost == game.solution) {
            game.status = GameStatus.SOLVED
            game = gameRepository.save(game)
        } else if (game.gamePosts!!.size >= numberOfRounds.toInt() - 1) {
            game.status = GameStatus.CLOSED
            game = gameRepository.save(game)
        }
        return game
    }

    private fun resolveGamePost(gamePost: GamePost, game: Game): GamePost {
        gamePost.helperSlots = getHelpSlots(gamePost.contentPost, game.solution)
        return gamePostRepository.save(gamePost)
    }
}
