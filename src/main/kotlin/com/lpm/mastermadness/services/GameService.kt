package com.lpm.mastermadness.services

import com.lpm.mastermadness.entities.Game
import com.lpm.mastermadness.entities.User
import com.lpm.mastermadness.repositories.GameRepository
import com.lpm.mastermadness.repositories.UserRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import kotlin.random.Random

@Service
class GameService {
    @Value("\${size.of.solution:}")
    lateinit var solutionSize: Number

    @Value("\${number.of.choices:}")
    lateinit var numberChoices: Number

    @Value("\${number.of.rounds:}")
    lateinit var numberRounds: Number

    @Autowired
    lateinit var gameRepository: GameRepository

    @Autowired
    lateinit var userRepository: UserRepository

    fun startGame(user: User): ResponseEntity<Game> {
        val game = gameRepository.save(Game(userRepository.save(user), getRandomSolution(),
                numberRounds.toInt(), numberChoices.toInt(), solutionSize.toInt()))
        //Client side don't have the solution
        game.solution = ""
        return ResponseEntity.ok(game)
    }

    private fun getRandomSolution(): String {
        return (1..numberChoices.toInt()).shuffled(Random.Default)
                .subList(0, solutionSize.toInt())
                .joinToString("")
    }
}